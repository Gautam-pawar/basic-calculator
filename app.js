
let num = "0";
let sum = 0;
let psymbol;

const screen = document.querySelector(".display h1")

function displaybtn(value) {

    if (isNaN(value)) {
        handleSymbol(value);
    } else {

        handleNumber(value);
    }
    view();
}

function handleNumber(number) {
    if (num === "0") {
        num = number;
    } else {
        num += number;
    }
    view(num);
}

function handleSymbol(symbol) {

    if (symbol == '0') {

    } else if (symbol == 'C') {

        num = "0";
        psymbol = null;
        sum = 0;

    } else if (symbol == '←') {

        if (num.length === 1) {
            num = "0";
        } else {

            num = num.substring(0, num.length - 1);

        }
    } else if (symbol == '=') {

        if (psymbol === null) {
            return;
        }

        calculate(parseInt(num));

        num = "" + sum;

        sum = 0;
        psymbol = null;

    } else if (symbol == '+' || symbol == '-' || symbol == '÷' || symbol == 'x') {

        dotheMath(symbol);

    }

}

function dotheMath(symbol) {
    if (num === '0') {
        return;
    }

    const tnum = parseInt(num);
    if (sum === 0) {
        sum = tnum;
    } else {
        calculate(tnum);
    }

    psymbol = symbol;
    num = "0";
}

function calculate(num1) {

    if (psymbol === "+") {
        sum += num1;
    } else if (psymbol === "-") {
        sum -= num1;
    } else if (psymbol === "x") {
        sum *= num1;
    } else if (psymbol === "÷") {
        sum /= num1;
    }
}

function view() {
    screen.innerText = num;
}

function cal() {
    document.querySelector(`.buttons`)
        .addEventListener("click", function (event) {
            displaybtn(event.target.innerText);
        });

}

cal();