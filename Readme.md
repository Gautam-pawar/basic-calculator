# Basic Calculator


## Hosting link : https://gautam-calculator.netlify.app/

## Built with Technologies 

* HTML
* CSS
* JavaScript


The following webpage is designed and built to perform basic mathematical operations like Addition, Subtraction, Multiplication & Division.

The project was assigned as practise drill for  HTML,CSS & Javascript by the Frontend Masters as part of the course curriculam.

The project is built using various refrences in the course as well as from outside resources.

we will be working on alternate approaches which we will be updating with the later versions on Gitlab.

